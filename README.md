# Podman / Docker

## Installation

Installation instructions are on [slides](slides.pdf) available in this repository (can be viewed from browser).

## Quickstart

You need to operate from run your Linux, MacOS or Windows terminal.
On Windows, we recommend `Powershell`.

_If you don't like `Powershell`, there's an [alternative](#alternative-to-powershell-on-windows)._

### In a terminal

You need to run this somewhere you can locate later, like ~/Documents or ~/Desktop.

__Note: `~/` means your home folder.__

```
docker pull quay.io/bughunting/client
mkdir bughunting
cd bughunting
docker run -dt --name bughunting-test -v"${PWD}:/home/bughunting/sources:z,rw" quay.io/bughunting/client
docker exec -ti bughunting zsh
```
You can copy and paste those commands all at once.

### HowTo

Detailed instructions for Bughunting can be found in our [HowTo](https://howto.bughunting.cz/).


## How to work with Docker container

### In a terminal

List directory contants, and change a directory.
```
ls
cd warm_up
ls
```

To autocomplete a command, use TAB.

If you want to use command-line code editor, use `nano file/to/edit.txt`.
You can save file with `CTRL+O` and enter, and exit it with `CTRL+X`.



### Outside of terminal

You can also go the the mounted folder bughunting on your computer, and use a graphical editor of your choice.

On Windows, if unsure, use:
https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v8.1.2/npp.8.1.2.Installer.exe

On Linux, if unsure, use `gedit`.

For the rest, or as an alternative, you can use [sublime text](https://www.sublimetext.com/download) (works for all OS).

# Alternative to Powershell on Windows

## Installation
Download and extract (unzip): https://github.com/cmderdev/cmder/releases/download/v1.3.18/cmder_mini.zip

## Usage

There're two differences:
  - Shell starts in folder where `cmder` is extracted. You need to `cd` to your ruby-container localtion.
  - When you're not yet inside the Docker container, you need to use `dir` instead of `ls` for listing folder contents.
  - You need to run the Docker container in a different way:

```
cd bughunting
docker run -dt --name bughunting -v"%cd%:/home/bughunting/sources:z,rw" quay.io/bughunting/client
```

Otherwise it should be the same.


# Alternative for Docker on Windows: SSH session **(best-support only)**

## Shell

For client, install: https://www.fosshub.com/KiTTY.html?dwl=kitty-0.74.4.13.exe

Please ask for the credentials in a private message.

## Remote folder (basically mounting)

You need 3 programs (open source ;))
 - https://github.com/evsar3/sshfs-win-manager/releases/download/v1.2.1/sshfs-win-manager-setup-v1.2.1.exe
 - https://github.com/billziss-gh/winfsp/releases/download/v1.9/winfsp-1.9.21096.msi
 - https://github.com/billziss-gh/sshfs-win/releases/download/v3.5.20357/sshfs-win-3.5.20357-x64.msi

_The last one is for 64bit; if it doesn't work for you, change the link from `x64` (at the end) to `x86`._

Please ask for the credentials in a private message.
